# Inventory service with site local preference

Our Dev's have deployed new service - **Inventory** to both DC and Cloud.

It is quieried by **API Service** and should be contacted directly in each Site where API is running:
![Inventory](img/MCN_7.png)

To enable and locally publish these services we need to:
* Create Pools
* Create HTTP LB
* Modify App Settings

## Create Pools

### Create DC Pool
* Manage -> Load Balancers -> Origin Pools -> Add Origin Pool:
* Name: **brews-inv-dc**
* Origin Servers: **Add Item**
    * Select Type of Origin Server: **IP address of Origin Server on given Sites**
    * IP: **10.1.1.\[your-number\]**
    * Site: **workshop-20221121-dc1**
    * Select Network on the site: **Outside Network**
    * **Apply**
* Port: **8002**
* Health Checks: **Add Item**
    * Select Item -> **Add Item**
        * Name: **inv-monitor**
        * Health Check: **HTTP Health Check**
        * Configured with default values: **Click** View Configuration
            * Path: **/api/stats**
            * **Apply**
        * **Continue**
* TLS: **Disable**
* **Save and Exit**
![inventory dc pool1](img/3-inv-dc-pool1.png)
![inventory monitor](img/3-inv-monitor.png)
![inventory dc pool2](img/3-inv-dc-pool2.png)

### Create AWS Pool
* Manage -> Load Balancers -> Origin Pools -> Add Origin Pool:
* Name: **brews-inv-aws**
* Origin Servers: **Add Item**
    * Select Type of Origin Server: **IP address of Origin Server on given Sites**
    * IP: **from table in [first cloud section](3.5-cloud2.md)**
    * Site: **truhlarna-aws**
    * Select Network on the site: **Outside Network**
    * **Apply**
* Port: **8002**
* Health Checks: **Add Item**
    * Select Item -> **inv-monitor**
* TLS: **Disable**
* **Save and Exit**

![inventory dc pool1](img/3-inv-aws-pool1.png)
![inventory dc pool2](img/3-inv-aws-pool2.png)

## Create HTTP Load Balancer

* Go to **Load Balancers -> Manage -> Load Balancers -> HTTP Load Balancers**
* Lets **Add HTTP Load Balancer**
* Name: **[your initials]-inventory-brews**
* Domains: **[your initials]-inventory.brews**
* Load Balancer Type: **HTTP**
* HTTP Port: **8002**
* Origin Pools: **Add Item**
    * Select **brews-inv-dc**
    * **Apply**
* Origin Pools: **Add Item**
    * Select **brews-inv-aws**
    * **Apply**    
* **Other Settings**
    * VIP Advertisment: **Custom**
    * **Configure**
        * Select Where to Advertise: **Site**
        * Site Network: Inside and Outside Network**
        * Site Reference: **workshop-20221121-dc1**
        * TCP Listen Port Choice: **Use Default TCP Listen Port**
        * **Apply**
    * **Add Item**
        * Select Where to Advertise: **Site**
        * Site Network: Inside and Outside Network**
        * Site Reference: **truhlarna-aws**
        * TCP Listen Port Choice: **Use Default TCP Listen Port**
        * **Apply**
* **Save and Exit**
![HTTP LB 1](img/3-inv-lb1.png)
![HTTP LB 1](img/3-inv-lb2.png)
![HTTP LB 1](img/3-inv-vip.png)
![HTTP LB 1](img/3-inv-lb3.png)

## Modify App Settings

Open your site thats published on HTTP Load Balancer and click (i) icon next to Shopping Cart:
![brews settings](img/3-brews-settings.png)

* Edit settings of **Inventory App**
* Set it to **http://[your initials]-inventory.brews:8002**
![inventory setting](img/3-inv-settings.png)

**Test your application**

... and have a beer. :)

