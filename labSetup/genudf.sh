#!/bin/bash

udfHost=543d60ac-8201-4d55-aeda-7d91bab4ae9e.access.udf.f5.com

for i in {47000..47031}
do
cat <<EOT >> configureudf-$i.sh
ssh -p $i $udfHost << EOF
curl -fsSL https://gitlab.com/jdbk/lab-setup/-/raw/main/xc-ws-labs23-udf.sh -o  xc-ws-labs23-udf.sh
sudo bash xc-ws-labs23-udf.sh

EOF
EOT

done

