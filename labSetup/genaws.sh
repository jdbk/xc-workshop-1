#!/bin/bash

file="awsips.txt"
awsips=$(cat $file)
key="~/Documents/LAB/access/aws-20221123.cer"
i=1
for ip in $awsips 
do
cat <<EOT > configureaws/caws-$i.sh
ssh -i  $key ubuntu@$ip << EOF
curl -fsSL https://gitlab.com/jdbk/lab-setup/-/raw/main/xc-ws-labs23-aws.sh -o  xc-ws-labs23-aws.sh
sudo bash xc-ws-labs23-aws.sh

EOF
EOT
let "i++"
done

