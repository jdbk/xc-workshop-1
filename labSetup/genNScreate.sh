#!/bin/bash

file="nss.txt"
nss=$(cat $file)
for ns in $nss 
do
cat <<EOT >> createns.sh
curl --key ~/.vescred/vesprivate.key --cert ~/.vescred/vescred.cert  -X 'POST' -H 'Content-Type: application/json' 'https://f5-cz.console.ves.volterra.io/api/web/namespaces' -d'{"metadata": {"annotations": {},"description": "ws20221121","disable": false,"labels": {},"name": "$ns","namespace": ""},"spec":{}}'
EOT

done



